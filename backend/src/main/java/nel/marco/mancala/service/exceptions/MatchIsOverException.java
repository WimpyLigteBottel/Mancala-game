package nel.marco.mancala.service.exceptions;

public class MatchIsOverException extends RuntimeException {
    public MatchIsOverException(String msg) {
        super(msg);
    }
}
